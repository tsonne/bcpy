//
// (C) SNSN 2017
// Author:Peter Schmidt
// Last edited: 2017-03-13
//

#include <stdint.h> 

#define BCMAGIC_1 7*19*23*113*511+8
#define BCMAGIC_2 13*17*113
#define BCMAGIC_3 13*17*113-169


typedef int16_t  int16;
typedef int32_t  int32;
typedef int64_t  int64;


/* container for content in bc-file */
typedef struct {
   int32 *data ;        /* pointer to waveform data, nData or 3*nData, depending on value of *component */
   int32 sTime ;        /* unix time for start of data, whole sec */
   int32 nData ;        /* number of samples, should be a multiple of freq */
   int32 cTime ;        /* unix time at creation of file, whole sec */
   char station[4] ;    /* name of station, 3 chars */
   char component[2] ;  /* z,n,e or 3 (3 components in order) */
   int16 freq ;         /* sample rate  (Hz) */
   int16 ascHeadSize ;  /* Number of bytes in ascii header (multiple of 4) */
   int16 ascHeadN ;     /* Number of lines in ascii header  -- */
   char *ascHead ;      /* pointer to ascii header data */
} BcFile;

/* Container for an entry in a bc index file */
typedef struct {
   char station[4];
   int32 sTime;
   int32 nSamp;
   int32 freq;
}  BcEntry;

/* Container for the content in a bc index file */
typedef struct {
   int32 n;
   int32 n_alloc;
   BcEntry *entry;
} BcIndex;


void init_BcFile(BcFile *obj);   /* initiates memory for a BcFile struct to 0 or NULL */
void init_BcIndex(BcIndex *obj); /* initiates memory for a BcIndex struct to 0 or NULL */

/* function realloc_BcFile() (re)allocates data and or ascHead in a BcFile if corresponding 
 * size(s) is > 0, 
 *  NOTE: old memory will not be touched but new will be initiated to 0 
 *  
 *  ARGUMENTS:
 *    obj        struct to (re)allocate
 *    nasc       lines in ascii header
 *    ndata      number of data points per component
 *    ncomp      number of components
 */
void realloc_BcFile(BcFile *obj, int nasc, int ndata, int ncomp);

/* function realloc_BcIndex() (re)allocates a BcIndex
 *
 *  ARGUMENTS
 *   obj        struct to (re)allocate
 *   size       to allocate to
 */
void realloc_BcIndex(BcIndex *obj, int size); 


void free_BcFile(BcFile *obj);   /* function free_BcFile() free's memory allocated for a BcFile struct, and re-initiates */
void free_BcIndex(BcIndex *obj); /* function free_BcIndex() free's memory allocated for a BcIndex struct, and re-initiates */


/* return True (1) if on little endian machine */
int is_LittleEndian_bc();


/* sorts the entries in a BcIndex struct */
void sort_BcIndex(BcIndex *obj);


/* function clean_BcIndex() removes duplicate entries in a BcIndex struct
 *
 * ARGUMENTS:
 *  obj        BcIndex to clean, will be sorted upon return if sorted is 0 upon 
 *              input
 *  sorted     if non-zero function will assume obj to be pre-sorted, hence only
 *              neighbouring entries will be compared, else obj will be sorted
 *              herein
 * 
 * RETURN
 *  number of entries removed from obj, NOTE: function will not shrink memory 
 *  allocated for obj
 */
int32 clean_BcIndex(BcIndex *obj, int sorted);


/* function read_bc() reads a bc file
 * 
 * ARGUMENTS:
 *  file      path to file to read
 *  res       will upon successful return hold the parsed content, will be allocated upon
 *             successful return, use free_BcFile() release memory when no longer needed
 *  mode      read mode, available values are:
 *             0   Read header only
 *             1   Read header and data
 *  
 * RETURN:
 *  function returns 0 upon successful return else one of the error codes
 *   -1    file could not be opened with read permissions
 *   -2    file is not a bc file (missing or erronous magic number)
 *   -3    failure to parse header block
 *   -4    failure to read/decode data block
 */
int read_bc(char *file, BcFile *res, int mode);


/* Function read_BcIndex() reads a bc index file 
 * 
 * ARGUMENTS:
 *  path        path to file to read
 *  content     allocatable array that upon return will
 *               hold the content in the index file, if 
 *               non-empty upon call new items will be 
 *               added
 * 
 * RETURN:
 *  if successful function will return the number of items 
 *  read from the index file, else:
 *   -1  file could not be opened with read permission
 *   -2  there were a problem reading the file
 *   -3  size of file differs from expected (i.e. file is probably not a bc index file)
 * 
 * (c) SNSN 2016
 * Author: Peter Schmidt
 */
int32 read_BcIndex(const char *path, BcIndex *content);


/* function write_bc() writes a bc file
 * 
 * ARGUMENTS:
 *  file      path to file to write
 *  sd        BcFile to write
 * 
 * RETURN:
 *  function returns 0 upon successful return else one of the error codes
 *   -1    file could not be opened with write permissions
 *   -2    failure to write header block
 *   -3    failure to write data block
 */
int write_bc(char *file, BcFile *sd);


/* Function write_BcIndex() writes a bc index file 
 * 
 * ARGUMENTS:
 *  path        path to file to write
 *  obj         BcIndex to write 
 * 
 * RETURN:
 *  if successful function will return 0, else:
 *   -1  file could not be opened with write permission
 *   -2  there was a problem writing the file
 *   -3  refusing to write empty file
 * 
 * (c) SNSN 2016
 * Author: Peter Schmidt
 */
int32 write_BcIndex(const char *path, BcIndex *obj); 