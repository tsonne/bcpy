import sys
from distutils.core import setup, Extension
if (sys.version_info.major == 2 and sys.version_info.minor < 4):
   module1 = Extension('BcPy', sources=['Bc_io.c', 'BcPy.c'],
                       define_macros=[(NODATETIME, None)])
else:
   module1 = Extension('BcPy', sources=['bc_io.c', 'BcPy.c'])
setup (name='BcPy',
       version='1.0',
       description='This package provides input/output functions for BC waveform files.',
       ext_modules=[module1])
