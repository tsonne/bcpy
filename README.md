# BcPy
* Purpose: Read/Write seismic waveform files in BC format with Python.
* This distribution should be compatible for both Python 2 and 3.
* Authors:
    * E. Kjartansson,
    * R. Bödvarsson,
    * A. Pharasyn,
    * P. Schmidt,
    * T. Sonnemann


## Installation of `BcPy`

Run:

```sh
python setup.py install
```

Then `BcPy` will be available as new module in Python.

## Example

```python3
from BcPy import BcRead
fn="/mnt/M2_1/DATA/VI/bc/2019/jul/15/mjo/00002600.bc"
data = BcRead(fn, mode=1, time='datetime')
for k in data:
    if k is not "data":
        print("{}: {}".format(k,data[k]))
    else:
        for c in data['data']:
            print("{}: {}".format(c,len(data['data'][c])))
```

