//  
//  Python utility to read and write bc files
//
//  compile python module with someting along (assuming bc_io.c is found in same folder): 
//  for python 2.x (x > 3): 
//           gcc BcPy.c bc_io.c -Wall -shared -fPIC -I/usr/include/python2.x -lpython2.x -lm -o BcPy.so
//  for python prior to 2.4 add -DNODATETIME (as datetime module did not exists earlier)
//
//  NOTE: In instruction above replace 2.x (in python2.x, 2 places) by the version of python you have 
//    installed (try: python -V if in doubt) and make sure that the path /usr/include/python2.x is
//    the proper path to the header file Python.h
//
//  Alternative to install BcPy globally for the current Python environment:
//      sudo python setup.py install
//  This uses the setup.py file and the Python distutils module.
//  In case the setup.py file is missing, here are its contents:
//  ```
//  from distutils.core import setup, Extension
//  module1 = Extension('BcPy', sources = ['bc_io.c','BcPy.c'])
//  setup (name = 'BcPy', version = '1.0',
//         description = 'input/output for BC waveform files.',
//         ext_modules = [module1])
//  ```
//
// (c) SNSN 2016-2020
// Last Edited: 2020-03-05
// 
// Authors: 
//   Einar Kjartansson  (1996-1999)
//   Reynir Bödvarsson  (1998-1999)
//   Arnaud Pharasyn    (2007-2010)
//   Peter Schmidt      (2014-    )
//   Tim Sonnemann      (2020)
//  
//   
//  LOG:
//   2017-01-11: Added "datetime" key to dict returned from BcRead (cps)
//   2017-02-16: Added "cdatetime" key to dict returned from BcRead and
//       implemented used of "datetime" and "cdatetime" as backup for
//       "start" and "ctime" respectively if not present in object given 
//       to BcWrite (cps) 
//   2017-02-17: Removed "datetime" and "cdatetime", instead added keyword
//       argument "time" to BcRead to switch between unix-time and datetime
//       object in output (under keys "start" and "ctime"), adjusted BcWrite
//       to test for input type. Implemented "time" keyword argument in 
//       BcIndexRead() and test for input format in BcIndexWrite()
//   2017-02-21: Minor edits to doc string of module
//   2017-03-13: Moved bc reader/writer code to stand alone C-library bc_io
//       Added releasing allocated memory in BcIndexWrite()
//   2017-03-27: Added some info to compilation instructions in this header
//   2017-04-07: Made sure keyword lists are NULL terminated, it seems that this
//      is not neccesary on python 2.7, but causes a BUG using python 2.6.6
//   2017-10-10: Minor edits to docstring of BcWrite()
//   2017-10-11: Fixed erronous use of printf() when wrinting error messages in BcWrite(),
//      now properly using sprintf(), additional edits to docstring of BcWrite(), among 
//      other things pointing out that input data arrays must be lists of integers.
//   2020-02-05: Made code compatible for both Python 2 and 3 by using
//      some preprocessor macros and conditions. New/changed places in code
//      indicated by comment with "Python2/3".
//      Also added comments about installation using setup.py. (ts)
//   2020-03-05: Adjusted parsing of input strings/unicode to char array as PyUnicode_[...]
//      family of functions (py3) does not do this automatically instead a PyObject is
//      returned that needs further treatment and proper reference decrement. 
//  

#ifndef MAIN

#ifndef NODATETIME
#define _POSIX_SOURCE
#endif

#include <Python.h>

#ifndef NODATETIME
#include <datetime.h>
#endif

#endif

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "bc_io.h"

// Python2/3 compatibility (ts)
#if PY_MAJOR_VERSION >= 3
   #define PyInt_FromLong PyLong_FromLong
   #define PyInt_AsLong PyLong_AsLong
   #define PyInt_Check PyLong_Check
   #define PyString_FromString PyUnicode_FromString
   #define PyString_Check PyUnicode_Check
   #define PyString_Size PyUnicode_GetLength
   static char _str_buff[513];
   char *pyString_toString(PyObject *pstr) {
      PyObject *pbyt = PyUnicode_AsASCIIString(pstr);
      strncpy(_str_buff,PyBytes_AsString(pbyt),513);
      Py_DECREF(pbyt);
      return _str_buff;
   }
#else
   #define pyString_toString PyString_AsString
#endif


#ifndef NODATETIME
/* helper function to convert datetime objects to unix time */
int32 datetime2unix(PyObject *dt) {
   int32 ut=0;
   struct tm ts;
   ts.tm_sec   = PyDateTime_DATE_GET_SECOND(dt);
   ts.tm_min   = PyDateTime_DATE_GET_MINUTE(dt);
   ts.tm_hour  = PyDateTime_DATE_GET_HOUR(dt);
   ts.tm_mday  = PyDateTime_GET_DAY(dt);
   ts.tm_mon   = PyDateTime_GET_MONTH(dt)-1;
   ts.tm_year  = PyDateTime_GET_YEAR(dt)-1900;
   ts.tm_isdst = 0;
   ut = (int32)mktime(&ts)-timezone;
   return ut;
}
#endif

#ifndef NODATETIME
static char BcIndexRead_doc[] = "\
   \r BcIndexRead(file, mode=2, time='unix')  -  function to read a bc-index file\n\n \
   \r INPUT\n\
   \r  file   name of file to read\n\
   \r  mode   reader mode, available values are:\n\
   \r          0    Just return entries as they were read\n\
   \r          1    Sort entries by start, nsamp, sps, sta\n\
   \r          2    1+Remove duplicate entries\n\n\
   \r  time   time format to return, available formats are\n\
   \r          'unix'      unix time (seconds since 1970-01-01 00:00:00)\n\
   \r          'datetime'  datetime object (UTC)\n\n\
   \r RETURN\n\
   \r upon successful return function will return a list of entries on format,\n\
   \r  [sta,start,nsamp,sps], where:\n\
   \r    sta   - 3-character station code\n\
   \r    start - UTC time of first data sample\n\
   \r    nsamp - number of data samples\n\
   \r    sps   - sampling rate (samples per sec.)\n\
   \r  else function returns None";
#else
static char BcIndexRead_doc[] = "\
   \r BcIndexRead(file, mode=2)  -  function to read a bc-index file\n\n \
   \r INPUT\n\
   \r  file   name of file to read\n\
   \r  mode   reader mode, available values are:\n\
   \r          0    Just return entries as they were read\n\
   \r          1    Sort entries by start, nsamp, sps, sta\n\
   \r          2    1+Remove duplicate entries\n\n\
   \r RETURN\n\
   \r upon successful return function will return a list of entries on format,\n\
   \r  [sta,start,nsamp,sps], where:\n\
   \r    sta   - 3-character station code\n\
   \r    start - unix time of first data sample\n\
   \r    nsamp - number of data samples\n\
   \r    sps   - sampling rate (samples per sec.)\n\
   \r  else function returns None";
#endif
static PyObject *BcIndexRead(PyObject *self, PyObject *args, PyObject *kwargs)  {
   BcIndex index;
   char err[128];      // error message string container
   const char *file=NULL;   // container for file path
   int i, mode=2;
   memset(err,0,128);
#ifndef NODATETIME
   PyObject *ret=NULL, *ent=NULL, *d=NULL;
   static const char *keywordList[] = {"file","mode","time",NULL};
   const char *time_format=NULL;
   int tf=0;
   struct tm ts;
   time_t tmp;
#else
   PyObject *ret=NULL, *ent=NULL;
   static const char *keywordList[] = {"file","mode",NULL};
#endif
   
   // parse argument list
#ifndef NODATETIME
   if (!PyArg_ParseTupleAndKeywords(args, kwargs, "s|is", (char **)keywordList, &file, &mode, &time_format)) {
      return NULL;
   } else if (time_format) {
      if      (strlen(time_format) == 4 && !strcmp("unix",time_format)) tf = 0;
      else if (strlen(time_format) == 8 && !strcmp("datetime",time_format)) tf = 1;
      else {
         sprintf(err,"BcRead() unknown value on argument time");
         PyErr_SetString(PyExc_TypeError, err);
         return NULL;
      }
   }
#else
   if (!PyArg_ParseTupleAndKeywords(args, kwargs, "s|i", keywordList, &file, &mode)) return NULL;
#endif
   
   // check that mode is in range
   if (mode < 0 || mode > 2) {
      sprintf(err,"GcfRead() unsuported value of input argument 2 (mode)");
      PyErr_SetString(PyExc_TypeError, err);
      return NULL;
   }
   
   // Initiate struct to hold content of file
   init_BcIndex(&index);
   
   // read the file
   if (read_BcIndex(file,&index) < 0) Py_RETURN_NONE;
   
   // adjust result according to mode
   if      (mode==1) sort_BcIndex(&index);
   else if (mode==2) clean_BcIndex(&index,0);
   
   // prepare return object
   ret = PyList_New(index.n);
   for (i=0; i<index.n; i++) {
      ent = PyList_New(4);
      PyList_SetItem(ent,0,PyString_FromString(index.entry[i].station));
#ifndef NODATETIME
      if (!tf) PyList_SetItem(ent,1,PyInt_FromLong((long)index.entry[i].sTime));
      else {
         // convert to datetime object
         tmp = (time_t)index.entry[i].sTime;
         gmtime_r((time_t *)&tmp,&ts);
         d = PyDateTime_FromDateAndTime((int)ts.tm_year+1900,(int)ts.tm_mon+1,(int)ts.tm_mday,(int)ts.tm_hour,(int)ts.tm_min,(int)ts.tm_sec,0);
         PyList_SetItem(ent,1,d);
      }
#else
      PyList_SetItem(ent,1,PyInt_FromLong((long)index.entry[i].sTime));
#endif
      PyList_SetItem(ent,2,PyInt_FromLong((long)index.entry[i].nSamp));
      PyList_SetItem(ent,3,PyInt_FromLong((long)index.entry[i].freq));
      PyList_SetItem(ret,i,ent);
   }
   
   // release allocated memory before returning
   free_BcIndex(&index);
   return ret;
}


#ifndef NODATETIME
static char BcIndexWrite_doc[] = "\
   \r BcIndexWrite(I, file)  -  function to write a bc-index file\n\n \
   \r INPUT\n\
   \r  I      list of entries on format [sta,start,nsamp,sps], where\n\
   \r          sta   - 3-character station code\n\
   \r          start - UTC time of first data sample, either unix time or\n\
   \r                   a datetime object\n\
   \r          nsamp - number of data samples\n\
   \r          sps   - sampling rate (samples per sec.)\n\
   \r  file   name of file to write\n\n\
   \r RETURN\n\
   \r  0 if file were successfully written else 1, function will raise a TypeError\n\
   \r  Exception upon erronous input.";
#else
static char BcIndexWrite_doc[] = "\
   \r BcIndexWrite(I, file)  -  function to write a bc-index file\n\n \
   \r INPUT\n\
   \r  I      list of entries on format [sta,start,nsamp,sps], where\n\
   \r          sta   - 3-character station code\n\
   \r          start - unix time of first data sample\n\
   \r          nsamp - number of data samples\n\
   \r          sps   - sampling rate (samples per sec.)\n\
   \r  file   name of file to write\n\n\
   \r RETURN\n\
   \r  0 if file were successfully written else 1, function will raise a TypeError\n\
   \r  Exception upon erronous input.";
#endif
static PyObject *BcIndexWrite(PyObject *self, PyObject *args)  {
   int ret=0,nargs,n;
   PyObject *I=NULL,*ent=NULL,*item;
   Py_ssize_t ni,i;
   char err[128];      // error message string container
   const char *file;   // container for file path
   BcIndex index;
   memset(err,0,128);
   
   // parse argument list
   if ((nargs=PyTuple_Size(args)) != 2) {
      sprintf(err,"BcIndexWrite() takes exactly two argument (%d given)",nargs);
      PyErr_SetString(PyExc_TypeError, err);
      return NULL;
   } else if (!PyArg_ParseTuple(args, "O!s",&PyList_Type, &I, &file)) {
      /* check that the proper types were input */
      return NULL;
   }
   
   // extract the entries to write
   if (!(ni = PyList_Size(I))) ret=1;   
   else {
      // initiate C-struct
      init_BcIndex(&index);
      realloc_BcIndex(&index,ni);
      for (i=0; i<ni; i++) {
         if (!(ent = PyList_GetItem(I,i))) sprintf(err,"BcIndexWrite() failed to retrieve item %d in input list",(int)i);
         else if (!PyList_Check(ent)) sprintf(err,"BcIndexWrite() erronous format of sub list %d in input list",(int)i);
         else if (PyList_Size(ent) != 4) sprintf(err,"BcIndexWrite() erronous size of sub list %d in input list",(int)i);
         else {
            if (!(item = PyList_GetItem(ent,0))) sprintf(err,"BcIndexWrite() failed to retrieve first item in sub list %d in input list",(int)i);
            else if (!PyString_Check(item)) sprintf(err,"BcIndexWrite() erronous type of first item in sub list %d in input list",(int)i);
            else strncpy(index.entry[i].station,pyString_toString(item),3);   
#ifndef NODATETIME
            if (!strlen(err)) {
               if (!(item = PyList_GetItem(ent,1))) sprintf(err,"BcIndexWrite() failed to retrieve second item in sub list %d in input list",(int)i);
               else if (PyInt_Check(item))      index.entry[i].sTime = (int32)PyInt_AsLong(item);
               else if (PyDateTime_Check(item)) index.entry[i].sTime = datetime2unix(item);
               else sprintf(err,"BcIndexWrite() erronous type of second item in sub list %d in input list",(int)i);
            } 
#else
            if (!strlen(err)) {
               if (!(item = PyList_GetItem(ent,1))) sprintf(err,"BcIndexWrite() failed to retrieve second item in sub list %d in input list",(int)i);
               else if (!PyInt_Check(item))   sprintf(err,"BcIndexWrite() erronous type of second item in sub list %d in input list",(int)i);
               else index.entry[i].sTime = (int32)PyInt_AsLong(item);
            }            
#endif
            if (!strlen(err)) {
               if (!(item = PyList_GetItem(ent,2))) sprintf(err,"BcIndexWrite() failed to retrieve third item in sub list %d in input list",(int)i);
               else if (!PyInt_Check(item))   sprintf(err,"BcIndexWrite() erronous type of third item in sub list %d in input list",(int)i);
               else index.entry[i].nSamp = (int32)PyInt_AsLong(item);
            }        
            if (!strlen(err)) {
               if (!(item = PyList_GetItem(ent,3))) sprintf(err,"BcIndexWrite() failed to retrieve fourth item in sub list %d in input list",(int)i);
               else if (!PyInt_Check(item)) sprintf(err,"BcIndexWrite() erronous type of fourth item in sub list %d in input list",(int)i);
               else index.entry[i].freq = (int32)PyInt_AsLong(item);
            }
         }
         if (strlen(err)) break;
      }
      index.n = ni;
      
      // write to file
      if ((n=write_BcIndex(file,&index))) {
         if (n==-1) sprintf(err,"BcIndexWrite() failed to open file with write permission");
         else if (n==-2) sprintf(err,"BcIndexWrite() failed to write file");
         else if (n==-3) sprintf(err,"BcIndexWrite() refusing to write empty file");
         else sprintf(err,"BcIndexWrite() some unknown problem occured while writing the file");
      }
   
      // release allocated memory before returning
      free_BcIndex(&index);
   }
   
   if (strlen(err)) {
      PyErr_SetString(PyExc_TypeError, err);
      return NULL;
   } else return PyInt_FromLong((long)ret);
}


#ifndef NODATETIME
static char BcRead_doc[] = "\
   \r BcRead(file, mode=1, time='unix')  -  function to read a bc file\n\n \
   \r INPUT\n\
   \r  file   name of file to read\n\
   \r  mode   reader mode, available values are:\n\
   \r          0    Read file header\n\
   \r          1    Read header and data\n\
   \r  time   time format to return, available formats are\n\
   \r          'unix'      unix time (seconds since 1970-01-01 00:00:00)\n\
   \r          'datetime'  datetime object (UTC)\n\n\
   \r RETURN\n\
   \r upon successful return function will return a dictionary on format:\n\
   \r   D['station']   - 3-character station code \n\
   \r   D['ctime']     - UTC time of file creation\n\
   \r   D['start']     - UTC time of first data sample\n\
   \r   D['sps']       - sampling rate (samples per sec.)\n\
   \r   D['n_data']    - number of data samples per data vector\n\
   \r   D['header']    - list of ascii header lines, may be empty\n\
   \r   D['header'][i] - ascii header line 1 as a string, typically there\n\
   \r                     are 3 header lines with the content:\n\
   \r                      ['SensorID DigitizerID',\n\
   \r                       'Creator_of_file',\n\
   \r                       'timing status']\n\
   \r                      Currently the total length of the ascii header is\n\
   \r                      restricted to 512 characters, including line-breaks\n\
   \r                      (one per line except last) and white spaces characters\n\
   \r   D['data']      - dictionary of data vectors, keys give the component\n\
   \r                     name. Typically all three components are present\n\
   \r   D['data'][c]   - (integer) list of data for component c\n\
   \r  else function returns None";
#else
static char BcRead_doc[] = "\
   \r BcRead(file, mode=1)  -  function to read a bc file\n\n \
   \r INPUT\n\
   \r  file   name of file to read\n\
   \r  mode   optional, available values are:\n\
   \r          0    Read file header\n\
   \r          1    Read header and data\n\n\
   \r RETURN\n\
   \r upon successful return function will return a dictionary on format:\n\
   \r   D['station']   - 3-character station code \n\
   \r   D['ctime']     - unix time of file creation\n\
   \r   D['start']     - unix time of first data sample\n\
   \r   D['sps']       - sampling rate (samples per sec.)\n\
   \r   D['n_data']    - number of data samples per data vector\n\
   \r   D['header']    - list of ascii header lines, may be empty\n\
   \r   D['header'][i] - ascii header line 1 as a string, typically there\n\
   \r                     are 3 header lines with the content:\n\
   \r                      ['SensorID DigitizerID',\n\
   \r                       'Creator_of_file',\n\
   \r                       'timing status']\n\
   \r   D['data']      - dictionary of data vectors, keys give the component\n\
   \r                     name. Typically all three components are present\n\
   \r   D['data'][c]   - (integer) list of data for component c\n\
   \r  else function returns None";
#endif
static PyObject *BcRead(PyObject *self, PyObject *args, PyObject *kwargs)  {
   char err[128];      // error message string container
   const char *file=NULL;   // container for file path
   PyObject *ret=NULL,*D=NULL, *data=NULL,*head=NULL,*d=NULL;
   BcFile obj;
   int32 *ptr, j;
   int i, n, mode=1;
#ifndef NODATETIME
   const char *time_format=NULL;
   static const char *keywordList[] = {"file","mode","time",NULL};
   int tf=0;
   struct tm ts;
   time_t tmp;
#else
   static const char *keywordList[] = {"file","mode",NULL};
#endif
   
   // parse argument list
#ifndef NODATETIME
   if (!PyArg_ParseTupleAndKeywords(args, kwargs, "s|is", (char **)keywordList, &file, &mode, &time_format)) return NULL;
   else if (time_format) {
      if      (strlen(time_format) == 4 && !strcmp("unix",time_format)) tf = 0;
      else if (strlen(time_format) == 8 && !strcmp("datetime",time_format)) tf = 1;
      else {
         sprintf(err,"BcRead() unknown value on argument time");
         PyErr_SetString(PyExc_TypeError, err);
         return NULL;
      }
   }
#else
   if (!PyArg_ParseTupleAndKeywords(args, kwargs, "s|i", keywordList, &file, &mode)) return NULL;
#endif
   
   // check that mode is in range
   if (mode != 0 && mode != 1) {
      sprintf(err,"BcRead() unsuported value of input argument 2 (mode)");
      PyErr_SetString(PyExc_TypeError, err);
      return NULL;
   }
   
   // Initiate struct to hold content of file
   init_BcFile(&obj);
   
   // read the file
   if (read_bc((char *)file,&obj,mode) != 0) Py_RETURN_NONE;
   
   // prepare return object
   ret = PyDict_New();
   d = PyString_FromString(obj.station);
   PyDict_SetItemString(ret,"station",d);
   Py_DECREF(d);
#ifndef NODATETIME
   if (!tf) {
      d = PyInt_FromLong((long)obj.sTime);
      PyDict_SetItemString(ret,"start",d);
      Py_DECREF(d);
      d = PyInt_FromLong((long)obj.cTime);
      PyDict_SetItemString(ret,"ctime",d);
      Py_DECREF(d);
   } else {
      tmp = (time_t)obj.sTime;
      gmtime_r((time_t *)&tmp,&ts);
      d = PyDateTime_FromDateAndTime((int)ts.tm_year+1900,(int)ts.tm_mon+1,(int)ts.tm_mday,(int)ts.tm_hour,(int)ts.tm_min,(int)ts.tm_sec,0);
      PyDict_SetItemString(ret,"start",d);
      Py_DECREF(d);
      tmp = (time_t)obj.cTime;
      gmtime_r((time_t *)&tmp,&ts);
      d = PyDateTime_FromDateAndTime((int)ts.tm_year+1900,(int)ts.tm_mon+1,(int)ts.tm_mday,(int)ts.tm_hour,(int)ts.tm_min,(int)ts.tm_sec,0);
      PyDict_SetItemString(ret,"ctime",d);
      Py_DECREF(d); 
   }
#else
   d = PyInt_FromLong((long)obj.sTime);
   PyDict_SetItemString(ret,"start",d);
   Py_DECREF(d);
   d = PyInt_FromLong((long)obj.cTime);
   PyDict_SetItemString(ret,"ctime",d);
   Py_DECREF(d);
#endif
   d = PyInt_FromLong((long)obj.freq);
   PyDict_SetItemString(ret,"sps",d);
   Py_DECREF(d);
   d = PyInt_FromLong((long)obj.nData);
   PyDict_SetItemString(ret,"n_data",d);
   Py_DECREF(d);
   head = PyList_New(obj.ascHeadN);
   n = 0;
   for (i=0; i<obj.ascHeadN; i++) {
      PyList_SetItem(head,i,PyString_FromString(obj.ascHead+n));
      n += strlen(obj.ascHead+n)+1;
   }
   PyDict_SetItemString(ret,"header",head);
   Py_DECREF(head);
   D = PyDict_New();
   if (mode) {
      if (strcmp(obj.component,"3")) {
         data = PyList_New(obj.nData);
         for (j=0; j<obj.nData; j++) PyList_SetItem(data,j,PyInt_FromLong((long)obj.data[j]));
         PyDict_SetItemString(D,obj.component,data);
         Py_DECREF(data);
      } else {
         for (i=0; i<3; i++) {
            data = PyList_New(obj.nData);
            ptr = obj.data+i*obj.nData;
            for (j=0; j<obj.nData; j++) {
               PyList_SetItem(data,j,PyInt_FromLong((long)*(ptr+j)));
            }
            if      (i==0) PyDict_SetItemString(D,"z",data);
            else if (i==1) PyDict_SetItemString(D,"n",data);
            else if (i==2) PyDict_SetItemString(D,"e",data);
            Py_DECREF(data);
         }
      }
   }
   PyDict_SetItemString(ret,"data",D);
   Py_DECREF(D);
   
   // release memory
   free_BcFile(&obj);
   return ret;
}


#ifndef NODATETIME
static char BcWrite_doc[] = "\
   \r BcWrite(D, file)  -  function to write a bc file\n\n \
   \r INPUT\n\
   \r  D      dictionary with metadata and data to write, on format:\n\
   \r   D['station']   - 3-character station code \n\
   \r   D['ctime']     - optional, UTC time of file creation, either unix time or\n\
   \r                     a datetime object, defaults to current time (UTC) if not set\n\
   \r   D['start']     - UTC time of first data sample, either unix time or a\n\
   \r                     datetime object\n\
   \r   D['sps']       - sampling rate (samples per sec.)\n\
   \r   D['header']    - optional, list of ascii header lines, may be empty or\n\
   \r                     excluded for no header but note that this may cause other\n\
   \r                     softwares than the read function in this module to fail\n\
   \r                     reading the file\n\
   \r   D['header'][i] - ascii header line 1 as a string, typically there\n\
   \r                     are 3 header lines with the content:\n\
   \r                      ['SensorID DigitizerID',\n\
   \r                       'Creator_of_file',\n\
   \r                       'timing status']\n\
   \r                     NOTE, empty lines will be output using a single blank\n\
   \r   D['data']      - dictionary of data vectors, keys should give the component\n\
   \r                     (z, n, e). Either 1 or all 3 components must be present.\n\
   \r                     Note however that using a single component may cause other\n\
   \r                     softwares than the read function in this module to fail\n\
   \r                     reading the file. Note, if 3 components are present data\n\
   \r                     vectors must be of equal length\n\
   \r   D['data'][c]   - (integer) list of data for component c\n\
   \r  file   name of file to write\n\n\
   \r RETURN\n\
   \r  0 if file were successfully written else 1, function will raise a TypeError\n\
   \r  Exception upon erronous input.";
#else
static char BcWrite_doc[] = "\
   \r BcWrite(D, file)  -  function to write a bc file\n\n \
   \r INPUT\n\
   \r  D      dictionary with metadata and data to write, on format:\n\
   \r   D['station']   - 3-character station code \n\
   \r   D['ctime']     - optional, unix time of file creation, if not set defaults\n\
   \r                     to current time (UTC)\n\
   \r   D['start']     - unix time of first data sample\n\
   \r   D['sps']       - sampling rate (samples per sec.)\n\
   \r   D['header']    - optional, list of ascii header lines, may be empty or\n\
   \r                     excluded for no header but note that this may cause other\n\
   \r                     softwares than the read function in this module to fail\n\
   \r                     reading the file\n\
   \r   D['header'][i] - ascii header line 1 as a string, typically there\n\
   \r                     are 3 header lines with the content:\n\
   \r                      ['SensorID DigitizerID',\n\
   \r                       'Creator_of_file',\n\
   \r                       'timing status']\n\
   \r                     NOTE, empty lines will be output using a single blank\n\
   \r   D['data']      - dictionary of data vectors, keys should give the component\n\
   \r                     (z, n, e). Either 1 or all 3 components must be present.\n\
   \r                     Note however that using a single component may cause other\n\
   \r                     softwares than the read function in this module to fail\n\
   \r                     reading the file. Note, if 3 components are present data\n\
   \r                     vectors must be of equal length\n\
   \r   D['data'][c]   - (integer) list of data for component c\n\
   \r  file   name of file to write\n\n\
   \r RETURN\n\
   \r  0 if file were successfully written else 1, function will raise a TypeError\n\
   \r  Exception upon erronous input.";
#endif
static PyObject *BcWrite(PyObject *self, PyObject *args)  {
   PyObject *D=NULL, *data=NULL, *head=NULL, *zcomp=NULL, *ncomp=NULL, *ecomp=NULL, *d=NULL;
   char err[128];      // error message string container
   const char *file;   // container for file path
   BcFile obj;
   int ret=0,nargs,n;
   int32 *ptr;
   Py_ssize_t nc,nd,nl,i,l;
   memset(err,0,128);
   
   // parse argument list
   if ((nargs=PyTuple_Size(args)) != 2) {
      sprintf(err,"BcWrite() takes exactly two argument (%d given)",nargs);
      PyErr_SetString(PyExc_TypeError, err);
      return NULL;
   } else if (!PyArg_ParseTuple(args, "O!s",&PyDict_Type, &D, &file)) {
      /* check that the proper types were input */
      return NULL;
   }
   // extract the entries to write
   if (!(data=PyDict_GetItemString(D,"data")))
      sprintf(err,"BcWrite() missing key \"data\" in input dict");
   else if (!PyDict_Check(data)) 
      sprintf(err,"BcWrite() erronous value on key \"data\" in input dict");
   else if ((nc=PyDict_Size(data)) != 1 && nc != 3)
      sprintf(err,"BcWrite() unsupported number of data components in input dict");
   else if ((nc==3 && (!PyDict_Contains(data,PyString_FromString("z")) || !PyDict_Contains(data,PyString_FromString("n")) || !PyDict_Contains(data,PyString_FromString("e")))) ||
            (nc==1 && (!PyDict_Contains(data,PyString_FromString("z")) && !PyDict_Contains(data,PyString_FromString("n")) && !PyDict_Contains(data,PyString_FromString("e")))))
      sprintf(err,"BcWrite() unknown component key in input dict");
   else {
      head=PyDict_GetItemString(D,"header");
      if (head && ! PyList_Check(head)) sprintf(err,"BcWrite() erronous value on key \"header\" in input dict");
      else {
         // so far so good
         init_BcFile(&obj);
         if (!(d=PyDict_GetItemString(D,"station"))) sprintf(err,"BcWrite() missing key \"station\" in input dict");
         else if (!PyString_Check(d)) sprintf(err,"BcWrite() erronous value on key \"station\" in input dict");
         else strncpy(obj.station,pyString_toString(d),3);
         
#ifndef NODATETIME
         if (!strlen(err)) {
            if (!(d=PyDict_GetItemString(D,"ctime"))) obj.cTime = (int32)time(NULL);
            else if (PyDateTime_Check(d)) obj.cTime = datetime2unix(d);
            else if (PyInt_Check(d)) obj.cTime = (int32)PyInt_AsLong(d);
            else sprintf(err,"BcWrite() erronous value on key \"ctime\" in input dict");
         }
         if (!strlen(err)) {
            if (!(d=PyDict_GetItemString(D,"start"))) sprintf(err,"BcWrite() missing key \"start\"|\"datetime\"  in input dict");
            else if (PyDateTime_Check(d)) obj.sTime = datetime2unix(d);
            else if (PyInt_Check(d)) obj.sTime = (int32)PyInt_AsLong(d);
            else sprintf(err,"BcWrite() erronous value on key \"start\" in input dict");
         }
#else
         if (!strlen(err)) {
            if (!(d=PyDict_GetItemString(D,"ctime"))) obj.cTime = (int32)time(NULL);
            else if (!PyInt_Check(d)) sprintf(err,"BcWrite() erronous value on key \"ctime\" in input dict");
            else obj.cTime = (int32)PyInt_AsLong(d);
         }
         if (!strlen(err)) {
            if (!(d=PyDict_GetItemString(D,"start"))) sprintf(err,"BcWrite() missing key \"start\" in input dict");
            else if (!PyInt_Check(d)) sprintf(err,"BcWrite() erronous value on key \"start\" in input dict");
            else obj.sTime = (int32)PyInt_AsLong(d);
         }
#endif
         if (!strlen(err)) {
            if (!(d=PyDict_GetItemString(D,"sps"))) sprintf(err,"BcWrite() missing key \"sps\" in input dict");
            else if (!PyInt_Check(d)) sprintf(err,"BcWrite() erronous value on key \"sps\" in input dict");
            else obj.freq = (int32)PyInt_AsLong(d);
         }
         if (!strlen(err)) {
            // convert the ascii header
            if (!head || !(nl=PyList_Size(head))) {
               obj.ascHeadSize = 0;
               obj.ascHeadN = 0;
            } else {         
               obj.ascHeadN = (int16)nl;
               // get size of ascii header (including separating null-terminators)
               n = nl;
               for (i=0; i<nl; i++) {
                  if (!(d=PyList_GetItem(head,i))) {
                     sprintf(err,"BcWrite() failed to retrieve line %d in ascii header in input dict",(int)i);
                     break;
                  } else if (!PyString_Check(d)) {
                     sprintf(err,"BcWrite() erronous type of item %d in ascii header in input dict",(int)i);
                     break;
                  } else {
                     l = PyString_Size(d);
                     if (!l) {
                        // set a blank
                        PyList_SetItem(head,i,PyString_FromString(" "));
                        l = 1;
                     }
                     n += l;
                  }
               }
               if (!strlen(err)) {
                  // adjust ascii header size to multiple of 4 and allocate 
                  n = 4*((n+3)/4);
                  realloc_BcFile(&obj,n,0,0);
                  
                  // assemble ascii header
                  n = 0;
                  for (i=0; i<nl; i++) {
                     d=PyList_GetItem(head,i);
                     sprintf(obj.ascHead+n,"%s",pyString_toString(d));
                     n += PyString_Size(d)+1;
                  }
               }
            }
            
            if (!strlen(err)) {
               // get the data
               if (nc==3) {
                  strncpy(obj.component,"3",1);
                  if (!(zcomp=PyDict_GetItemString(data,"z"))) sprintf(err,"BcWrite() failed to retrieve data component z from input dict");
                  else if (!(ncomp=PyDict_GetItemString(data,"n"))) sprintf(err,"BcWrite() failed to retrieve data component n from input dict");
                  else if (!(ecomp=PyDict_GetItemString(data,"e"))) sprintf(err,"BcWrite() failed to retrieve data component e from input dict");
                  else if (!PyList_Check(zcomp)) sprintf(err,"BcWrite() erronous type of data array for z component in input dict, must be a list");
                  else if (!PyList_Check(ncomp)) sprintf(err,"BcWrite() erronous type of data array for n component in input dict, must be a list");
                  else if (!PyList_Check(ecomp)) sprintf(err,"BcWrite() erronous type of data array for e component in input dict, must be a list");
                  else if (!(nd=PyList_Size(zcomp))) sprintf(err,"BcWrite() empty data array for z component in input dict");
                  else if (PyList_Size(ncomp) != nd || PyList_Size(ecomp) != nd) sprintf(err,"BcWrite() data array in input dict are not of equal length");
                  else {
                     // allocate space for data vector
                     realloc_BcFile(&obj,0,nd,3);
                     
                     // get the data
                     for (i=0; i<nd; i++) {
                        if (!(d=PyList_GetItem(zcomp,i))) {
                           sprintf(err,"BcWrite() failed to retrieve data %d for z component in input dict",(int)i);
                           break;
                        } else if (!PyInt_Check(d)) {
                           sprintf(err,"BcWrite() erronous type of data %d for z component in input dict, must be an integer",(int)i);
                           break;
                        } else obj.data[i] = (int32)PyInt_AsLong(d);                        
                     }
                     ptr = obj.data+nd;
                     for (i=0; i<nd; i++) {
                        if (!(d=PyList_GetItem(ncomp,i))) {
                           sprintf(err,"BcWrite() failed to retrieve data %d for n component in input dict",(int)i);
                           break;
                        } else if (!PyInt_Check(d)) {
                           sprintf(err,"BcWrite() erronous type of data %d for n component in input dict, must be an integer",(int)i);
                           break;
                        } else ptr[i] = (int32)PyInt_AsLong(d);                        
                     }
                     ptr += nd;
                     for (i=0; i<nd; i++) {
                        if (!(d=PyList_GetItem(ecomp,i))) {
                           sprintf(err,"BcWrite() failed to retrieve data %d for e component in input dict",(int)i);
                           break;
                        } else if (!PyInt_Check(d)) {
                           sprintf(err,"BcWrite() erronous type of data %d for e component in input dict, must be an integer",(int)i);
                           break;
                        } else ptr[i] = (int32)PyInt_AsLong(d);                        
                     }
                  }
               } else {
                  if      (PyDict_Contains(data,PyString_FromString("z"))) strncpy(obj.component,"z",1);
                  else if (PyDict_Contains(data,PyString_FromString("n"))) strncpy(obj.component,"n",1);
                  else if (PyDict_Contains(data,PyString_FromString("e"))) strncpy(obj.component,"e",1);
                  if (!(zcomp=PyDict_GetItemString(data,obj.component))) sprintf(err,"BcWrite() failed to retrieve data component from input dict");
                  else if (!PyList_Check(zcomp)) sprintf(err,"BcWrite() erronous type of data array in input dict");
                  else if (!(nd=PyList_Size(zcomp))) sprintf(err,"BcWrite() empty data array in input dict");
                  else {
                     // allocate space for data vector
                     realloc_BcFile(&obj,0,nd,1);
                     
                     // get the data
                     for (i=0; i<nd; i++) {
                        if (!(d=PyList_GetItem(zcomp,i))) {
                           sprintf(err,"BcWrite() failed to retrieve data %d in input dict",(int)i);
                           break;
                        } else if (!PyInt_Check(d)) {
                           sprintf(err,"BcWrite() erronous type of data %d in input dict",(int)i);
                           break;
                        } else obj.data[i] = (int32)PyInt_AsLong(d);
                     }
                  }
               }
            }
         }
         
         if (!strlen(err)) {
            // write to file
            if ((n=write_bc((char *)file,&obj))) {
               if (n==-1) sprintf(err,"BcWrite() failed to open file with write permission");
               else if (n==-2) sprintf(err,"BcWrite() failed to write bc header");
               else if (n==-3) sprintf(err,"BcWrite() refusing to write bc data");
               else sprintf(err,"BcWrite() some unknown problem occured while writing the file");
            }
         }
         
         // release memory
         free_BcFile(&obj);
      }
   }
   
   if (strlen(err)) {
      PyErr_SetString(PyExc_TypeError, err);
      return NULL;
   } else return PyInt_FromLong((long)ret);
}


/* Method table */
static PyMethodDef BcPy_methods[] = {
   {"BcIndexRead",  (PyCFunction)BcIndexRead,  METH_VARARGS | METH_KEYWORDS, BcIndexRead_doc},
   {"BcIndexWrite", (PyCFunction)BcIndexWrite, METH_VARARGS,                 BcIndexWrite_doc},
   {"BcRead",       (PyCFunction)BcRead,       METH_VARARGS | METH_KEYWORDS, BcRead_doc},
   {"BcWrite",     (PyCFunction)BcWrite,       METH_VARARGS,                 BcWrite_doc},
   {NULL, NULL, 0, NULL}
};

// store BcPy doc string based on datetime outside of init (ts)
#ifndef NODATETIME
static char BcPy_doc[] = "\
   \r python module for reading and writing bc data/index files \n\
   \r available functions:\n\
   \r  BcIndexRead(file, mode=2, time='unix')  -  read a bc index file\n\
   \r  BcIndexWrite(I, file)                   -  write a bc index file\n\
   \r  BcRead(file, mode=1, time='unix')       -  read a bc file\n\
   \r  BcWrite(D, file)                        -  write a bc file";
#else
static char BcPy_doc[] = "\
   \r python module for reading and writing bc data/index files \n\
   \r available functions:\n\
   \r  BcIndexRead(file, mode=2)  -  read a bc index file\n\
   \r  BcIndexWrite(I, file)      -  write a bc index file\n\
   \r  BcRead(file, mode=1)       -  read a bc file\n\
   \r  BcWrite(D, file)           -  write a bc file";
#endif

// Python2/3 compatibility (ts)
#if PY_MAJOR_VERSION >= 3
   #define MOD_ERROR_VAL NULL
   #define MOD_SUCCESS_VAL(val) val
   #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
   #define MOD_DEF(ob, name, doc, methods) \
           static struct PyModuleDef moduledef = { \
             PyModuleDef_HEAD_INIT, name, doc, -1, methods, }; \
           ob = PyModule_Create(&moduledef);
#else
   #define MOD_ERROR_VAL
   #define MOD_SUCCESS_VAL(val)
   #define MOD_INIT(name) void init##name(void)
   #define MOD_DEF(ob, name, doc, methods) \
           ob = Py_InitModule3(name, methods, doc);
#endif

/* Initializer */
// using macros to account for Python2/3 (ts)
MOD_INIT(BcPy) {
   PyObject *mod;
#ifndef NODATETIME
   PyDateTime_IMPORT;
#endif
   MOD_DEF(mod, "BcPy", BcPy_doc, BcPy_methods)
   if (mod == NULL)
       return MOD_ERROR_VAL;
   // NOTE: should PyModule_AddObject() be checked here?
   return MOD_SUCCESS_VAL(mod);
}
