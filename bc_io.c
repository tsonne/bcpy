//  
//  C-library to read and write bc files
//
//  compile shared library with  : gcc bc_io.c -Wall -shared -fPIC -g -o bc_io.so -lm
//  compile stand-alone tool with: gcc bc_io.c -Wall -g -o BcTool -lm -DMAIN
//  
//  LOG:
//    2017-03-13   Lifted library out of BcfPy.c to stand alone library
//    2017-09-19   Updated cmp_bc() to compare all lines in ascii-header, previously only
//        first lines were. Defaulted BcTool (main()) to assume bc file if file type can
//        not be determined 
//    2019-04-12   Corrected minor issue in main (handling of mode for index file were wrong)
//
// (c) SNSN 2016-2019
// Last Edited: 2019-04-12
// 
// Authors: 
//   Einar Kjartansson  (1996-1999)
//   Reynir Bödvarsson  (1998-1999)
//   Arnaud Pharasyn    (2007-2010)
//   Peter Schmidt      (2014-    )


#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <fcntl.h> 
#include <time.h> 
#include <sys/stat.h>
#include "bc_io.h"

#define BCMAGIC_1 7*19*23*113*511+8
#define BCMAGIC_2 13*17*113
#define BCMAGIC_3 13*17*113-169


/* struct used while (de)compressing data*/
typedef struct {
    char *data;       // bit buffer for compressed data
    int32 bytes;      // size of bit buffer
    int32 bits;       // bit-size of work
    int64 work;       // temporary work buffer while (de)compressing data
    int32 dMask[32];  // bit masks, tMask[i] should hold 1 in i, else 0, used to detect negative numbers while decompressing 
    int32 rMask[32];  // bit masks, rMask[i] should hold 1 in 0-i, else 0, used to mask bit-buffer/data while (de)compressing
    int32 lMask[32];  // bit masks, lMask[i] should hold 1 in i-32, else 0, used to add negative sign while decompressing and
                      //   detecting change of bits used per data
    int64 rMask64[64];   // bit masks used while decoding 32 bit number in 64 bit buffer
} BitBuffer_n ;

/* struct to hold a bc-datablock */
typedef struct {
    int32 size;       // size of struct once data is allocated, i.e. size of data is (size-12)
    int32 nData;      // number of data samples
    int16 magic;      // magic number BCMAGIC_2
    char  preProc;    // preprocessing: 0 - nothing, 1 - first bcDiff, 2 - second cpDiff (only 1 is used herein)
    char  type;       // compression type: 0 - nothing, 1 - 8,16,32 bits ,2 - 1..24 bits (only 2 is used herein)
    char  *data;      // bit buffer for compressed data
} BcDataBlock;

/* outputs content in a BcEntry */
void printf_BcEntry(BcEntry *entry) {
   struct tm *st;
   char t[20];
   time_t tt = entry->sTime;
   st = gmtime(&tt);
   strftime(t,20,"%Y-%m-%dT%H:%M:%S",st);
   printf("%s: %ld Hz, %s, %ld samples\n",entry->station,(long)entry->freq,t,(long)entry->nSamp);
}

/* outputs content in a BcIndex */
void printf_BcIndex(BcIndex *obj) {
   int i;
   printf("%ld entries\n",(long)obj->n);
   for (i=0; i<obj->n; i++) printf_BcEntry(&obj->entry[i]);
}

/* outputs content of BcFile in same style as bctool -p option */
void printf_BcFile(BcFile *bc) {
   int i,j;
   char sbuff[25],cbuff[25];
   struct tm ts, tc;   
   time_t t2,t1;
   
   // set up time strings
   t1 = bc->sTime; // NOTE: as time_t may be of greater size then bc->[s|c]Time we can not simply recast as accesing
   t2 = bc->cTime; //  the value then may read out of intended memory
   gmtime_r((time_t *)&t1,&ts);  // NOTE: By some reason using gmtime did not work here, instead the second
   gmtime_r((time_t *)&t2,&tc);  //  call over-wrote the memory location of the first call...
   strftime(sbuff,25,"%a %b %e %H:%M:%S %Y",&ts);
   strftime(cbuff,25,"%a %b %e %H:%M:%S %Y",&tc);
   
   // output
   printf(" %s %s %s\n",bc->station,bc->component,sbuff);
   printf(" %d samples at %d samples/sec\n",bc->nData,bc->freq);
   printf(" Time of creation: %s\n",cbuff);
   i = 0;
   for (j=0; j<bc->ascHeadN; j++) {
      printf(" \"%s\"\n",bc->ascHead+i);
      i += 1+strlen(bc->ascHead+i);
   }
   if (bc->data) {
      i = bc->nData;
      printf("  %6ld %6ld %6ld %6ld %6ld  -- %6ld %6ld %6ld %6ld\n",
             (long)bc->data[0],(long)bc->data[1],(long)bc->data[2],(long)bc->data[3],(long)bc->data[4],
             (long)bc->data[i-4],(long)bc->data[i-3],(long)bc->data[i-2],(long)bc->data[i-1]);
      fflush(stdout);
      if (!strcmp(bc->component,"3")) {
         printf("  %6ld %6ld %6ld %6ld %6ld  -- %6ld %6ld %6ld %6ld\n",(long)bc->data[i],(long)bc->data[i+1],(long)bc->data[i+2],
                (long)bc->data[i+3],(long)bc->data[i+4],(long)bc->data[2*i-4],(long)bc->data[2*i-3],
                (long)bc->data[2*i-2],(long)bc->data[2*i-1]);
         fflush(stdout);
         printf("  %6ld %6ld %6ld %6ld %6ld  -- %6ld %6ld %6ld %6ld\n",(long)bc->data[2*i],(long)bc->data[2*i+1],
                (long)bc->data[2*i+2],(long)bc->data[2*i+3],(long)bc->data[2*i+4],(long)bc->data[3*i-4],
                (long)bc->data[3*i-3],(long)bc->data[3*i-2],(long)bc->data[3*i-1]); 
         fflush(stdout);
      }
   }
}
/* function init_BcFile() initiates memory for a BcFile struct to 0 or NULL */
void init_BcFile(BcFile *obj) {
   obj->data = NULL;
   obj->sTime = 0;
   obj->cTime = 0;
   obj->nData = 0;
   memset(obj->station,'\0',4);
   memset(obj->component,'\0',2);
   obj->freq = 0;
   obj->ascHeadSize = 0;
   obj->ascHeadN = 0;
   obj->ascHead = NULL;
}


/* function init_BitBuffer() prepares the BitBuffer and bit masks used for compressing and decompressing
 * bc data
 * 
 * ARGUMENTS:
 *  b           BitBuffer to use
 *  data        storage of compessed data
 */
void init_BitBuffer(BitBuffer_n *b, char *data) {
   int i;
   int32 r=1,l=-1;
   int64 r64=1;
   
   // init the bitbuffer
   b->bytes = 0; 
   b->bits  = 0; 
   b->work  = 0; 
   b->data  = data;
   
   // set up the bit masks
   for(i = 0 ; i < 32 ; ++i) {
      b->rMask[i] = r;
      b->lMask[i] = l;
      b->dMask[i] = l & r;
      r += r + 1;
      l <<= 1;
   }
   
   // set up the 64 bit masks
   for(i = 0 ; i < 64 ; ++i) {
      b->rMask64[i] = r64;
      r64 += r64 + 1;
   }
}
/* inits a BcIndex */
void init_BcIndex(BcIndex *obj) {
   obj->n = 0;
   obj->n_alloc = 0;
   obj->entry = (BcEntry *)NULL;
}
/* inits a BcEntry */
void init_BcEntry(BcEntry *obj) {
   memset(obj->station,0,4);
   obj->sTime = 0;
   obj->nSamp = 0;
   obj->freq = 0;
}
/* function realloc_BcFile() reallocates data and or ascHead in a BcFile if corresponding size(s) is > 0, 
   NOTE: old memory will not be touched but new will be initiated to 0 */
void realloc_BcFile(BcFile *obj, int nasc, int ndata, int ncomp) {
   if (ndata*ncomp > 0 && ndata != obj->nData) {
      obj->data = (int32 *)realloc(obj->data,ndata*ncomp*sizeof(int32));
      if (ndata*ncomp > obj->nData) memset(obj->data+obj->nData,0,(ndata*ncomp-obj->nData)*sizeof(int32));
      obj->nData = ndata;
   }
   if (nasc > 0 && nasc != obj->ascHeadSize) {
      obj->ascHead = (char *)realloc(obj->ascHead,nasc*sizeof(char));
      if (nasc > obj->ascHeadSize) memset(obj->ascHead,'\0',(nasc-obj->ascHeadSize));
      obj->ascHeadSize = nasc;
   }
}
/* (re)allocates a BcIndex*/
void realloc_BcIndex(BcIndex *obj, int size) {
   int i;
   if (size > 0 && size != obj->n_alloc) {
      obj->entry = (BcEntry *)realloc(obj->entry,size*sizeof(BcEntry));
      if (size > obj->n_alloc) {
         for (i=obj->n_alloc; i<size; i++) init_BcEntry(&obj->entry[i]);
      }
      obj->n_alloc = size;
      if (size < obj->n) obj->n = size;
   }
}
/* function free_BcFile() free memory allocated for a BcFile struct, and re-initiates */
void free_BcFile(BcFile *obj) {
   if (obj->data)    free(obj->data);
   if (obj->ascHead) free(obj->ascHead);
   init_BcFile(obj);
}
/* free's a BcIndex */
void free_BcIndex(BcIndex *obj) {
   if (obj->n_alloc>0 && obj->entry) {
      free(obj->entry);
      obj->entry = (BcEntry *)NULL;
      obj->n_alloc=0;
      obj->n=0;
   }
}
/* compares content of two BcEntry structs */
int comp_BcEntry(BcEntry *e1, BcEntry *e2) {
   int d,f=100000;
   if ((d = strcmp(e1->station,e2->station))) return d*f;
   if ((d = e1->freq-e2->freq)) return d*f;
   if (e1->sTime == e2->sTime) return (int)(e1->nSamp-e2->nSamp);
   return (int)(e1->sTime-e2->sTime);
}
/* sorts the entries in a BcIndex struct */
void sort_BcIndex(BcIndex *obj) {
   if (obj->n > 0) qsort(obj->entry,obj->n,sizeof(BcEntry),(int(*)())comp_BcEntry);
}


/* function clean_BcIndex() removes duplicate entries in a BcIndex struct */
int32 clean_BcIndex(BcIndex *obj, int sorted) {
   int32 i,n;
   
   n = 0;
   if (!sorted)  sort_BcIndex(obj);
   for (i=1; i<obj->n; i++) {
      if (!comp_BcEntry(&obj->entry[i-1],&obj->entry[i])) n++;
      else if (n) {
         strncpy(obj->entry[i-n].station,obj->entry[i].station,sizeof(obj->entry[i].station));
         obj->entry[i-n].sTime = obj->entry[i].sTime;
         obj->entry[i-n].nSamp = obj->entry[i].nSamp;
         obj->entry[i-n].freq  = obj->entry[i].freq;
      }
   }
   if (n) {
      for (i=obj->n-n;i<obj->n;i++) init_BcEntry(&obj->entry[i]);
      obj->n -= n;
   }
   return n;
}


/* function add_BcIndex() inserts a BcEntry to a BcIndex if not already present
 * 
 * ARGUMENTS:
 *   obj        BcIndex to add BcEntry to
 *   ent        BcEntry to add
 *   sorted     if non-zero function will assume obj to be pre-sorted, else obj
 *               will be sorted herein
 * 
 * RETURN:
 *  index in obj->entry where ent where inserted, -1 if already present
 */
int add_BcIndex(BcIndex *obj, BcEntry ent, int sorted) {
   int i,d;
   
   if (!sorted) sort_BcIndex(obj);
   for (i=0;i<obj->n; i++) {
      if ((d = strcmp(obj->entry[i].station,ent.station)) > 0) break;
      else if (!d) {
         if ((d = obj->entry[i].freq-ent.freq) > 0) break;
         else if (!d) {
         if ((d = obj->entry[i].sTime-ent.sTime) > 0) break;
            else if (!d) {
               if ((d = obj->entry[i].nSamp-ent.nSamp) > 0) break;
               else if (!d) {
                  i = -1;
                  break;
               }
            }
         }
      }   
   }
   if (i==obj->n) realloc_BcIndex(obj,obj->n_alloc+1);
   if (i>-1) {
      if (obj->n == obj->n_alloc) realloc_BcIndex(obj,obj->n_alloc+1);
      if (i<obj->n) memmove(obj->entry+i+1,obj->entry+i,(obj->n-i)*sizeof(BcEntry));
      strncpy(obj->entry[i].station,ent.station,sizeof(ent.station));
      obj->entry[i].sTime = ent.sTime;
      obj->entry[i].nSamp = ent.nSamp;
      obj->entry[i].freq  = ent.freq;
      obj->n++;
   }
   return i;
}


/* function del_BcIndex() deletes a BcEntry in a BcIndex if present
 * 
 * ARGUMENTS:
 *   obj        BcIndex to delete BcEntry from
 *   ent        BcEntry to remove
 * 
 * RETURN:
 *  index in obj->entry where ent were found, else -1
 *  NOTE: function will note shrink memory 
 *  allocated for obj
 */
int del_BcIndex(BcIndex *obj, BcEntry ent) {
   int i,j=-1;   
   for (i=0;i<obj->n; i++) {
      if (!strcmp(obj->entry[i].station,ent.station) && !(obj->entry[i].freq-ent.freq) && !(obj->entry[i].sTime-ent.sTime) && !(obj->entry[i].nSamp-ent.nSamp)) {
         if (i<obj->n) {
            memmove(obj->entry+i,obj->entry+i+1,(obj->n-(i+1))*sizeof(BcEntry));
            init_BcEntry(&obj->entry[obj->n-1]);
         }
         obj->n--;
         j = i;
      }   
   }
   return j;
}


/* return True (1) if on little endian machine */
int is_LittleEndian_bc() {
   static int i = -192; // Just an initial value to compute only once if little endian
   if (i == -192) {
      int j;
      char *cp;
      cp = (char *) & j;
      j = 57;
      i = (*cp == 57);
   }
   return i;
}


/* shift all bytes in long = 32 bit integer here, hmmm why is the masking needed here (simply to assure we work with 8 bit) */
void swab_long(char *val) {
   char temp;
   temp = *val & 0xff;
   *val = *(val+3) & 0xff;
   *(val+3) = temp & 0xff;
   temp = *(val+1) & 0xff;
   *(val+1) = *(val+2) & 0xff;
   *(val+2) = temp & 0xff;
}


/* function swab_many copies n bytes from from src to dest
 * while swapping adjecent bytes (basically changing 
 * endianess if swab_many points at an array of 2 byte, short,
 * data). Function is same as swab in unistd.h but allows for 
 * src and dest to be the same
 * 
 * ARGUMENTS:
 *  src        where to copy bytes from
 *  dest       where to copy bytes to, may be the same as src
 *  n          number of bytes in src, should be an even number
 */
void swab_many(void *src, void *dest, ssize_t n) {
   int i=0;
   char t[2],*s,*d;
   s = (char *)src;
   d = (char *)dest;
   while (i<n) {
      t[1] = s[i];
      i++;
      if (i == n) break;
      t[0] = s[i];
      d[i-1] = t[0];
      d[i]   = t[1];
      i++;
   }
}


/* function get_bcBits() retrieves and uncompresses a data from a BitBuffer
 * 
 * ARGUMENTS:
 * 
 * RETURN
 *  function returns the uncompressed data 
 */
int32 get_bcBits(BitBuffer_n *b, int nbits, int endian) {
   int32 res;
   char *cp;

   // adapt to endianess
   if (endian) cp = (char *) &(b->work);
   else        cp = (char *) &(b->work) + 7;

   // fill up work respository, NOTE: buffer should at maximum hold 7 bits
   // upon entry, hence we will always load 32 bits here
   while (b->bits < 32) {
      b->work <<= 8;
      *cp = b->data[b->bytes++];
      b->bits += 8;
   }
   
   // retrieve and uncompress (
   b->bits -= nbits;
   res = b->rMask[nbits-1] & (b->work >> b->bits);
   
   // adjust sign
   if (res & b->dMask[nbits-1]) res |= b->lMask[nbits-1];

   return res;
}


/* function put_bcBits() appends a signed integer to the bit buffer
 * 
 * NOTE: last value appended to bit buffer may need to be flushed from
 *  work repository to bit buffer, to do so call function a last time 
 *  with a dummy value (0 prefferably) and nbits=8. Further algorithm 
 *  is only guaranteed to properly compress values that can be represented
 *  by 25 bits or less (-16777215 to 16777215) (but may work for greater/
 *  lower values)
 * 
 * ARGUMENTS
 *   b           BitBuffer
 *   val         value to append
 *   nbits       number of bits to represent the value with
 *   endian      0 if on BIG endian machine else non-zero
 */
void put_bcBits(BitBuffer_n *b, int32 val, int nbits, int endian) {
   char *cp;
   
   // prepend the bits representing the number to the work repository
   b->work |= ((val & b->rMask64[nbits-1]) << (64-b->bits-nbits));
   b->bits += nbits;

   // set a pointer to the last/lowest byte in the work repository
   if (endian) cp = (char *)&(b->work)+7;
   else        cp = (char *)&(b->work);
   
   // move full bytes from work repository to the bit buffer
   while (b->bits >= 8) {
      b->data[b->bytes++] = *cp;
      b->work  <<= 8;
      b->bits -= 8;
   }
}


/* function level_minima() flattens local minima in a curve by setting
 * all values lower than the maximum value within a given search-distance
 * equal to the maximum value
 * 
 * ARGUMENTS:
 *  inp       array giving the curve to flatten
 *  out       array to hold the flattened curve, may be the same as inp
 *  n         size of inp
 *  len       search distance/flattening width
 * 
 * NOTE: Flattening algorithm have been modified slightly from original flattening 
 *  algorithm to avoid the need for an extra data point in the inp array, the 
 *  modification may affect out, for the original algorithm un-comment the commented
 *  code line below
 */
void level_minima(int *inp, int *out, int32 n, int len) {
   int32 i=0;
   int j,ll,aold=999,a,amax;

   // loop over the array
   while(i < n) {
      // get next value
      a = inp[i];
      if (aold <= a) {
         // next value is greater or equal than previously set low, simply pass and reset low
         aold = a;
         out[i++] = a;
      } else {
         // next value is lower than previous low, look ahead to see if there is a larger value
         // within search radius
         ll = n-i;
         if (ll > len) ll = len;
         amax = a;
         for (j = 1; j < ll; j++) {
            if (inp[i+j] > amax) amax = inp[i+j];
         }
         if (i+j == n) amax = 999;  // this line replaces the previous one-extra-element demand on inp 
         if (amax > a) {
            // yes there were a larger value hence we must have a local minima, flatten it
            if (aold > amax) aold = amax;
            while(i<n && inp[i] <= a) out[i++] = aold ;
         } else {
            // no there are not hence we have not yet reached the bottom of the minima
            aold = a;
            out[i++] = a;
         }
      }
   }
}


/* function level_slopes() approximates a smooth curve by
 * stair-case curve whose steps always intercept at least one 
 * point of the original curve and never are at lower value.
 * optimally invoke function bcMagFilt() prior to this 
 * 
 * ARGUMENTS
 *  inp       curve to approximate
 *  out       array to put result in, may be same as inp
 *  n         size of inp
 *  len       desired width of steps (also maximum size)
 *  
 * NOTE: The original algorithm has been slightly modified as it 
 *  most likely contained a BUG causing the second half of the
 *  algorithm to do nothing. To get the original algorithm back
 *  uncomment the commented code line below 
 */
void level_slopes(int *inp, int *out, int32 n, int len) {
   int32 i,ii,i2=0;
   int a,a2=inp[0];
   
   // loop through input array forwards
   for(i=0; i<n; i++) {
      a = inp[i];
      out[i] = a;
      // check if we are on a slope 
      if(a!=a2) {
         // check that the section to fill in is not to long
         // and that the slope is positive
         if(((i-i2)<len) && (a>a2)) {
            // NOTE: this algorithm is a bit slow here as we will
            //  repeatable change the value of a given point until
            //  either the slope becommes negative or the section 
            //  to long
            for(ii=i2; ii<i; ii++) out[ii] = a ;
            a2 = a ;
         } else {
            a2 = a ;
            i2 = i ;
         }
      }
   }
   
   // loop through input array backwards and repeat algorithm above
   a2 = out[n-1]; 
   i2 = n-1;
   for(i=n-1; i>-2; i--) {
//       a = out[i];
      a=0; // NOTE: this basically disables the algorithm
      if(a != a2) {
         if(((i2-i) < len) && (a>a2)) {
            for(ii=i+1; ii<=i2; ii++) out[ii]=a;
            a2=a;
         } else {
            a2=a;
            i2=i;
         }
      }
   }
}


/* function get_compBits() estimates the minimum number of bits needed to  
 * store the signed integers in an array assuming the first bit of each
 * integer is reserved for the sign
 *
 * ARGUMENT
 *  inp      array of integers to compute bit size of
 *  out      array to put bitsize into
 *  n        size of array inp
 */
void get_compBits(int32 *inp, int *out, int32 n) {
   int i,v,r,a ;
   for(i=0; i<n ; i++) {
      a = abs(inp[i]) ;
      v = 1 ;
      r = 1 ;
      while( v <= a ) { 
         r++; 
         v += v;
      }
      out[i] = r ;
   }
}


/* function integrate_bc() performs a cummulative integration of a data array
 * 
 * ARGUMENTS:
 *  inp       data array to integrate_bc
 *  out       data array to put result in, may be same as inp
 *  n         size of inp and out
 * 
 */
void integrate_bc(int32 *inp,int32 *outp,int32 n) {
   int32 sum = 0;
   if(n<1) return;
   while(n--) {
      sum += *inp++;
      *outp++ = sum;
   }    
}


/* function differentiate_bc() computes the first difference of a data array
 * retaining the original value (integration constant) in first position
 * 
 * ARGUMENTS
 *  inp       array to compute first difference of
 *  outp      array to store first difference in, may be the same as input array
 *  n         size of inp
 */
void differentiate_bc(int32 *inp, int32 *outp, int32 n) {
   int32 old=0,d;
   if (n < 1) return;
   while(n--) {
      d = *inp++ ;
      *outp++ = d - old ;
      old = d ;
   }
}


/* function compress_bc() compresses a data vector
 * 
 * NOTE: outd needs to be corrected for endianess before output if 
 *  neccesary, input endian is only used by compression algorithm
 * 
 * ARGUMENTS:
 *  ind       data array to compress
 *  n         size of array
 *  outd      container to put compressed array in
 *  nDiff     order of difference to take prior to decompression
 *            (0 - None, 1 - first difference, 2 - second diff, ...)
 *  endian    0 if on BIG endian machine else non-zero
 * 
 * RETURN:
 *  function returns size of datablock
 */
int compress_bc(int32 *ind, int32 n, BcDataBlock *outd ,int nDiff, int endian) {
   int j, *cbits, m,om=-1;
   int32 *tmp,*data,ss,i,d;
   BitBuffer_n bb;
   
   // allocate memory
   tmp = (int32 *)calloc(n,sizeof(int32)) ;
   cbits = (int *)calloc(n,sizeof(int32));
   data = ind;
   
   // preprocess data
   for(j=0; j<nDiff; j++) {
      differentiate_bc(data,tmp,n); 
      data = tmp; 
   }
   
   // get the number of bits needed 
   get_compBits(data,cbits,n);
   
   // adjust the number of bits in to segments of equally many bits
   level_minima(cbits,cbits,n,20);
   level_slopes(cbits,cbits,n,20);
   
   // initiate BitBuffer and set up bit masks 
   init_BitBuffer(&bb,outd->data);
   
   // loop over the data vector
   for (i=0; i<n; i++) {
      m = cbits[i];
      d = data[i];
      // check if number of bits needed to represent the current number have changed
      if (om != m) {
         // they have, if not the first number set flag that number of bits needed to represent
         // tralling numbers will change
         if (i) put_bcBits(&bb,bb.dMask[om-1],om,endian);
         // write info on number of bits needed to represent the comming numbers 
         put_bcBits(&bb,m,6,endian);
         om = m;
      }
      put_bcBits(&bb,d,m,endian);
   }

   // flush remaining bits in work repository, if any, to buffer
   if (bb.bits) put_bcBits(&bb,0,8,endian);
   
   // add a 0-byte to guarantee short alignment which is needed to set the ending magic number in proper place
   outd->data[bb.bytes]=0;
   
   // round the size of (outd + magic number) to an integer number of short's
   ss = (12+bb.bytes+1)/2;
   
   // set the datablock info
   outd->size = 2*(ss+1); 
   outd->magic = BCMAGIC_2;
   outd->nData = n;
   outd->preProc = nDiff;
   outd->type = 2;
   
   // add a magic number at the end of the compressed data
   *((int16 *)(outd->data+outd->size-14)) = BCMAGIC_3;   

   // release allocated memory before exit
   free(tmp);
   free(cbits);
   return outd->size;
}


/* function decompress_bc() decompresses bc-compressed data
 * 
 * ARGUMENTS:
 *  buff      buffer holding the compressed data
 *  data      array to put the un-compressed data into
 *  n         size of data, i.e. expected number of data in buff
 *  endian    0 if on BIG endian machine else non-zero
 */
void decompress_bc(char *buff, int32 *data, int32 n, int endian) {
   BitBuffer_n bb;
   int32 m, i, d; 
  
   // initiate the bitbuffer and bit masks
   init_BitBuffer(&bb, buff);
   
   // get the number of bits used for the first data
   m = get_bcBits(&bb,6,endian);
   
   // loop over the expected number of data to un-compress
   for (i=0; i<n; i++) {
      // uncompress next data
      d = get_bcBits(&bb,m,endian);
      // look for flag that number of bits to use is to change
      if(d==bb.lMask[m-1]) {
         // get new number of bits to use for each data point
         m = get_bcBits(&bb,6,endian);
         
         // uncompress next data
         d = get_bcBits(&bb,m,endian);
      }
      // set uncompressed data into output array
      data[i] = d ;
   }
}

   
/* function read_bcDataBlock() reads a data block from a bc file
 * 
 * ARGUMENTS:
 *   fid        stream to read from, should be positioned at start
 *                of data block to read (i.e. directly after header 
 *                block or previous data block)
 *   block      will upon successful return hold the data block
 *   endian     0 if on BIG endian machine else non-zero
 * 
 * RETURN:
 *  if successful function returns 0 else function returns one of the error codes:
 *   -1    failed to read data block
 *   -2    erronous header in data block
 *   -3    failed to verify data block
 */
int read_bcDataBlock(FILE *fid, BcDataBlock *block, int endian) {
   int ret=0;
   
   // read the header
   if ((1-fread(&block->size,4,1,fid)) ||
       (1-fread(&block->nData,4,1,fid)) ||
       (1-fread(&block->magic,2,1,fid)) ||
       (1-fread(&block->preProc,1,1,fid)) ||
       (1-fread(&block->type,1,1,fid))) ret=-1;
   else {
      if (endian) swab_long((char *)&block->size);
      if (block->size < 14) ret=-2;
      else {
         // allocate memory         
         block->data = (char *)malloc(block->size-12);
         block->size = block->size;
         
         // read data block
         if ((block->size-12-fread(block->data, 1, block->size-12, fid))) {
            ret=-1;
            free(block->data);
            block->data = NULL;
         } else {         
            // correct endianess if needed
            if (endian) {
               swab_long((char *)&block->nData);
               swab_many(&block->magic,&block->magic,2);
               swab_many(block->data+block->size-14,block->data+block->size-14,2);
            }
            
            // verify data block
            if (block->magic != BCMAGIC_2 ||
             *((int16 *)(block->data+block->size-14)) != BCMAGIC_3 ||
             block->preProc < 0 ||
             block->type != 2) {
               free(block->data);
               block->data = NULL;
               ret = -3;
            }
         }
      }
   }
   return ret;
}


/* function write_bcDataBlock() writes a BcDataBlock to file 
 * 
 * ARGUMENTS:
 *  fid        stream to write to
 *  block      BcDataBlock to write
 *  endian     0 if on BIG endian machine else non-zero
 * 
 * RETURN:
 *  function returns 0 if successful else a non-zero integer
 */
int write_bcDataBlock(FILE *fid, BcDataBlock *block, int endian) { 
   char *buff ;
   int32 nw,nb=block->size;
   
   // allocate buffer and copy content to write
   buff = (char *) malloc(nb);
   memcpy(buff,block,12);
   memcpy(buff+12,block->data,nb-12);
   
   // adjust header for endianess   
   if(endian) {
      swab_long(buff);
      swab_long(buff+4);
      swab_many(buff+8 , buff+8 ,2);
      swab_many(buff+nb-2,buff+nb-2,2); 
   }  
   
   // write buffer then release allocated memory
   nw = fwrite(buff,1,nb,fid) ;
   free(buff) ;
   return(nb-nw);
}


/* function read_bc() reads a bc file */
int read_bc(char *file, BcFile *res, int mode) {
   int i, j, n, ndata, nasc, ret=0;
   FILE *fid;
   BcDataBlock block; 
   int32 *ip=NULL;
   int32 magicNumber;
   int endian = is_LittleEndian_bc();
   
   // open the file
   if (!(fid = fopen(file,"r"))) ret = -1;
   else if ((1-fread(&magicNumber, 4, 1, fid))) ret = -2;
   else  {
      // check out the magic number
      if (endian) swab_long((char *)&magicNumber);
      if (magicNumber != BCMAGIC_1) ret = -2;
      else {
         // read the static part of the header
         if ((1-fread(&res->sTime,4,1,fid)) ||
             (1-fread(&res->nData,4,1,fid)) ||
             (1-fread(&res->cTime,4,1,fid)) ||
             (1-fread(&res->station,4,1,fid)) ||
             (1-fread(&res->component,2,1,fid)) ||
             (1-fread(&res->freq,2,1,fid)) ||
             (1-fread(&res->ascHeadSize,2,1,fid)) ||
             (1-fread(&res->ascHeadN,2,1,fid))) ret = -3;
         else {
            // adjust endianess if needed
            if (endian) {
               swab_long((char*)&res->sTime);
               swab_long((char*)&res->nData);
               swab_long((char*)&res->cTime); 
               swab_many((char*)&(res->freq),(char*)&(res->freq),6);
            }
            // allocate memory
            if (!strcmp(res->component,"3")) n = 3;
            else n = 1;
            ndata = res->nData;
            nasc = res->ascHeadSize;
            res->ascHeadSize = 0;
            if (mode) {
               res->nData = 0;
               realloc_BcFile(res,nasc,ndata,n);
            } else realloc_BcFile(res,nasc,0,0);
               
            // load ascii header
            if (res->ascHeadSize) {
               if ((1-fread(res->ascHead, res->ascHeadSize, 1, fid))) {
                  ret = -3;
                  free_BcFile(res);
               }
            }
            if (!ret && mode) {
               // read and parse data vectors
               ip = res->data ;
               for(i=0; i<n; i++) {
                  if (read_bcDataBlock(fid,&block,endian)) {
                     ret = -4;
                     break;
                  }
                  // decompress
                  decompress_bc(block.data,ip,block.nData,endian);
                  
                  // integrate_bc
                  for(j=0; j<block.preProc; j++) integrate_bc(ip,ip,block.nData);
                  
                  // adjust pointer and free memory
                  ip += res->nData;
                  free(block.data);
               }
               if (ret) free_BcFile(res);
            }
         }
      }
      fclose(fid);
   }
   return ret;
}


/* function write_bc() writes a bc file */
int write_bc(char *file, BcFile *sd) {
   FILE *fid;
   BcDataBlock block;
   BcFile outh=*sd;
   int32 bcmagic=BCMAGIC_1;
   int nd=outh.nData,ret=0;
   int endian = is_LittleEndian_bc();
   
   // open the file
   if (!(fid=fopen(file,"w"))) ret=-1;
   else {
      // adjust endianess for header
      if (endian) {
         swab_long((char *)&bcmagic);
         swab_long((char *)&outh.sTime);
         swab_long((char *)&outh.cTime);
         swab_long((char *)&outh.nData);
         swab_many((char*)&(sd->freq),(char*)&(outh.freq),6) ;
      }
      
      // write header to file
      if ((1-fwrite(&bcmagic,4,1,fid)) ||
          (1-fwrite(&outh.sTime,4,1,fid)) ||
          (1-fwrite(&outh.nData,4,1,fid)) ||
          (1-fwrite(&outh.cTime,4,1,fid)) ||
          (1-fwrite(outh.station,4,1,fid)) ||
          (1-fwrite(outh.component,2,1,fid)) ||
          (1-fwrite(&outh.freq,2,1,fid)) ||
          (1-fwrite(&outh.ascHeadSize,2,1,fid)) ||
          (1-fwrite(&outh.ascHeadN,2,1,fid)) ||
          (sd->ascHeadSize && (1-fwrite(sd->ascHead,sd->ascHeadSize,1,fid)))) ret=-2;
      else {
         // Allocate storage for bit buffer
         block.data = (char *)malloc((nd+2)*sizeof(int32)+2);
         // compress and write data blocks to file
         compress_bc(sd->data,nd,&block,1,endian);
         if (!write_bcDataBlock(fid,&block,endian)) {
            if (!strcmp(outh.component,"3")) {
               compress_bc(sd->data+nd,nd,&block,1,endian);
               if (!write_bcDataBlock(fid,&block,endian)) {
                  compress_bc(sd->data+2*nd,nd,&block,1,endian) ;
                  if (write_bcDataBlock(fid,&block,endian)) ret=-3;
               } else ret=-3;
            } 
         } else ret=-3;
         // free allocated memory
         free(block.data);
      }
      
      // close file and ulink if something went wrong
      fclose(fid);
      if(ret) unlink(file);
   }
   return ret;
}


/* Function read_BcIndex() reads a bc index file */
int32 read_BcIndex(const char *path, BcIndex *content) {
   int fid,endian=is_LittleEndian_bc();
   int32 n=-1, size, ret, m, i;

   /* open file */
   fid = open(path, O_RDONLY);
   if (fid > -1) {
      if ((size = lseek(fid,0,SEEK_END)) > -1) {
         if ((ret  = lseek(fid,0,SEEK_SET)) > -1) {
            n = size/sizeof(BcEntry);
            if (n*(int32)sizeof(BcEntry) != size) n = -3;
            else if (n>0) {
               m = content->n;
               realloc_BcIndex(content,m+n);
               ret = read(fid,content->entry+m,size);
               if (ret != size) {
                  n = -2;
                  if (!m) free_BcIndex(content);
                  else realloc_BcIndex(content,m);
               } else {
                  content->n = m+n;
                  if (endian) {
                     for (i=m; i<m+n; i++) {
                        swab_long((char *)&content->entry[i].sTime);
                        swab_long((char *)&content->entry[i].nSamp);
                        swab_long((char *)&content->entry[i].freq);
                     }
                  }
               }
            }
         }
      }
      
      close(fid);
   }
   return n;
}


/* Function write_BcIndex() writes a bc index file */
int32 write_BcIndex(const char *path, BcIndex *obj) {
   int fid;
   int32 i,ret=-1;
   BcIndex tmp;
   int endian = is_LittleEndian_bc();
   
   init_BcIndex(&tmp);
   if (!obj->n) ret = -3;
   else if ((fid = open(path,O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) >= 0) {
      if (endian) {
         // need to convert endianess but do so onlocal copy
         realloc_BcIndex(&tmp,obj->n);
         for (i=0; i<obj->n; i++) {
            strncpy(tmp.entry[i].station,obj->entry[i].station,sizeof(obj->entry[i].station));
            tmp.entry[i].sTime = obj->entry[i].sTime;
            tmp.entry[i].nSamp = obj->entry[i].nSamp;
            tmp.entry[i].freq  = obj->entry[i].freq;
            swab_long((char *)&tmp.entry[i].sTime);
            swab_long((char *)&tmp.entry[i].nSamp);
            swab_long((char *)&tmp.entry[i].freq);
         }
         tmp.n = obj->n;
         if (write(fid,tmp.entry,tmp.n*sizeof(BcEntry)) != tmp.n*(int32)sizeof(BcEntry)) ret = -2;
         else ret = 0;
         free_BcIndex(&tmp);
      } else {
         if (write(fid,obj->entry,obj->n*sizeof(BcEntry)) != obj->n*(int32)sizeof(BcEntry)) ret = -2;
         else ret = 0;
      }
      close(fid);
   }
   return ret;
}


#ifdef MAIN
/* function cmp_bc() compares the content in two BcFile struct 
 *
 * ARGUMENTS:
 *  b1        BcFile 1
 *  b2        BcFile 2
 *  v         if none-zero function will output result to stdout, else function
 *              will be silent
 *
 * RETURN:
 *  function will return 0 if no differences were found between files, note that
 *  this also applies to the case were b2 contains no data but b1 do, else none-
 *  zero will be returned
 */
int cmp_bc(BcFile *b1, BcFile *b2, int v) {
   int i,j,n,err=0,cmp_dat=1,head_diff=0;
   if (strcmp(b1->station,b2->station)) {
      if (v) printf(" Station differs: \"%s\"/\"%s\"\n",b1->station,b2->station);
      err = 1;
      cmp_dat = 0;
   }
   if (strcmp(b1->component,b2->component)) {
      if (v) printf(" Components differs: \"%s\"/\"%s\"\n",b1->component,b2->component);
      err = 1;
      cmp_dat = 0;
   }
   if (b1->cTime - b2->cTime) {
      if (v) printf(" Creation time differs: %ld/%ld\n",(long)b1->cTime,(long)b2->cTime);
      err = 1;
   }
   if (b1->sTime - b2->sTime) {
      if (v) printf(" Start time differs: %ld/%ld\n",(long)b1->sTime,(long)b2->sTime);
      err = 1;
      cmp_dat = 0;
   }   
   if (b1->nData - b2->nData) {
      if (v) printf(" Number of data differs: %ld/%ld\n",(long)b1->nData,(long)b2->nData);
      err = 1;
      cmp_dat = 0;
   }   
   if (b1->freq - b2->freq) {
      if (v) printf(" Frequency differs: %d/%d\n",b1->freq,b2->freq);
      err = 1;
      cmp_dat = 0;
   }   
   if (b1->ascHeadSize - b2->ascHeadSize) {
      if (v) printf(" Ascii header size differs: %d/%d\n",b1->ascHeadSize,b2->ascHeadSize);
      err += 2;
   }   
   if (b1->ascHeadN - b2->ascHeadN) {
      if (v) printf(" Number of lines in ascii header differs: %d/%d\n",b1->ascHeadN,b2->ascHeadN);
      err += 2;
   }
   if (err < 2) {
      i = 0;
      for (j=0; j<b1->ascHeadN; j++) {
         if (strcmp(b1->ascHead+i,b2->ascHead+i)) {
            head_diff = 1;
            break;
         } else i += 1+strlen(b1->ascHead+i);
      }
      if (head_diff) {
         err += 1;
         if (v) {
            printf(" Ascii header differs:\n");
            printf("  file 1:\n");
            i = 0;
            for (j=0; j<b1->ascHeadN; j++) {
               printf("   \"%s\"\n",b1->ascHead+i);
               i += 1+strlen(b1->ascHead+i);
            }
            printf("  file 2:\n");
            i = 0;
            for (j=0; j<b2->ascHeadN; j++) {
               printf("   \"%s\"\n",b2->ascHead+i);
               i += 1+strlen(b2->ascHead+i);
            }
         }
      }
   }
//    if (err < 2 && strcmp(b1->ascHead,b2->ascHead)) {
//       if (v) printf(" Ascii header differs:\n");
//       if (v) printf(" file 1: \"%s\"\n",b1->ascHead);
//       if (v) printf(" file 2: \"%s\"\n",b2->ascHead);
//       
//    } else if (err>1) err -= 2;
   if (cmp_dat) {
      if (!strcmp(b1->component,"3")) n = 3*b1->nData;
      else n = b1->nData;
      if (b2->data && b1->data) {
         for (i=0; i<n; i++) {
            if (b1->data[i] - b2->data[i]) {
               if (v) printf(" Data point %d differs: %ld/%ld\n",i,(long)b1->data[i],(long)b2->data[i]);
               err = 1;
               break;
            }
         }
      } else if (v) printf(" No data read, hence cannot compare\n");
   }
   if (!err && v) printf(" Content in files is the same\n");
   return err;
}


/* function cmp_BcIndex() compares the content in two BcIndex Structs
 * 
 * ARGUMENTS:
 *  b1        BcIndex 1
 *  b2        BcIndex 2
 *  v         if none-zero function will output result to stdout, else function
 *              will be silent
 *
 * RETURN:
 *  function will return 0 if no differences were found between files.
 */
int cmp_BcIndex(BcIndex *b1, BcIndex *b2, int v) {
   int ret=0;
   int32 n, i;
   if (b1->n!=b2->n) {
      ret = 1;
      if (v) printf(" Number of entries differ in files: %ld/%ld\n",(long)b1->n,(long)b2->n);
      if (b1->n>b2->n) n = b2->n;
      else n = b1->n;
   } else n = b1->n;
   for (i=0; i<n; i++) {
      if (strcmp(b1->entry[i].station,b2->entry[i].station)) printf(" Station code differs in entry %d: \"%s\"/\"%s\"\n",i,b1->entry[i].station,b2->entry[i].station);
      if (b1->entry[i].sTime-b2->entry[i].sTime) printf(" Start differs in entry %d: %ld/%ld\n",i,(long)b1->entry[i].sTime,(long)b2->entry[i].sTime);
      if (b1->entry[i].nSamp-b2->entry[i].nSamp) printf(" Number of samples differs in entry %d: %ld/%ld\n",i,(long)b1->entry[i].nSamp,(long)b2->entry[i].nSamp);
      if (b1->entry[i].freq-b2->entry[i].freq) printf(" Sampling rate differs in entry %d: %ld/%ld\n",i,(long)b1->entry[i].freq,(long)b2->entry[i].freq);
   }
   if (!ret && v) printf(" Content in files is the same\n");
   return ret;
}


/* main utility */
int main(int argc, char *argv[]) {
   int ret=0,mode=1,task=0,ftype=0,i,n,mset=0;
   char *f1=NULL, *f2=NULL;
   BcFile bf1, bf2;
   BcIndex bi1, bi2;
   
   if (argc < 2) {
      printf("\n\
        \r usage: %s [opts] file [file2] \n\n\
        \r where:\n\
        \r file    primary file to use\n\
        \r file2   second file to use or output to, must not be\n\
        \r          the same as file, if specified and neither -cp\n\
        \r          not -cmp option is specified -cmp will be \n\
        \r          implicitly assumed\n\
        \r opts    optional options, if none is specified script\n\
        \r           will output content of file to stdout\n\n\
        \r optional options are:\n\
        \r  -m=n     read mode where n should be one of:\n\
        \r              0    bc data files only, read header only\n\
        \r              1    default, read full file as is\n\
        \r              2    bc index files only, sort entries\n\
        \r              3    bc index files only, sort and remove\n\
        \r                    duplicate entries\n\
        \r  -f=type   where type can be either bc or index. If not\n\
        \r             used function will try to determine file type\n\
        \r             from (1) -mode, (2) file name, (3) assume it\n\
        \r             is a bc file\n\
        \r  -cmp      compare content in files\n\
        \r  -cp       copy file to file2 (-m=0 is not allowed)\n\n",argv[0]);
      ret = 1;
   } else {
      // parse input
      for (i=1; i<argc; i++) {
         if (argv[i][0] != '-') break;
         n = strlen(argv[i]);
         if (n==4 && !strncmp(argv[i],"-cmp",n)) {
            if (task && task != 1) {
               printf(" You cannot combine -cmp with other options setting the task to do, aborting\n");
               ret=1;
               break;
            }
            task = 1;
         } else if (n==3 && !strncmp(argv[i],"-cp",n)) {
            if (task && task != 2) {
               printf(" You cannot combine -cp with other options setting the task to do, aborting\n");
               ret=1;
               break;
            }
            task = 2;
         } else if (n>4 && !strncmp(argv[i],"-f=",3)) {
            if (n==5 && !strncmp(argv[i],"-f=bc",n)) ftype = 1;
            else if (n==8 && !strncmp(argv[i],"-f=index",n)) ftype = 2;
            else {
               printf(" Unsupported file type \"%s\"\n",argv[i]+3);
               ret = 1;
               break;
            }
         } else if (n==4 && !strncmp(argv[i],"-m=",3)) {
            mset = 1;
            if (argv[i][3]=='0') mode = 0;
            else if (argv[i][3]=='1') mode = 1;
            else if (argv[i][3]=='2') mode = 2;
            else if (argv[i][3]=='3') mode = 3;
            else {
               printf(" Unsupported read mode: %c\n",argv[i][3]);
               ret = 1;
               break;
            }
         } else {
            printf(" Unsupported option \"%s\", aborting\n",argv[i]);
            ret = 1;
         }
      }
      
      // check input and set implicit values if needed
      if (!ret) {
         if (i==argc) {
            printf(" No file(s) specified, aborting\n");
            ret = 1;
         } else if (i<argc-2) {
            printf(" To many files specified, aborting\n");
            ret = 1;
         } else if (!task) {
            if (i<argc-1) {
               task = 1;
               f1 = argv[i];
               f2 = argv[i+1];
            } else {
               f1 = argv[i];
            }
         } else {
            if (i==argc-1) {
               printf(" Two files are nedded for selected task, aborting\n");
               ret = 1;
            } else {
               f1 = argv[i];
               f2 = argv[i+1];
            }
         }
         
         if (!ret) {
            if (f2 && !strcmp(f2,f1)) { 
               printf(" Files must not be the same, aborting\n");
               ret = 1;
            } else if (!ftype) {
               if (mset && mode != 1) {
                  if (!mode) ftype = 1;
                  else ftype = 2;
               } else {
                  n = strlen(f1);
                  if (n>3 &&  !strncmp(f1+n-3,".bc",3)) ftype = 1;
                  else if (n>4 &&  !strncmp(f1+n-5,"index",5)) ftype = 2;
               }
               if (!ftype && f2) {
                  n = strlen(f2);
                  if (n>3 &&  !strncmp(f2+n-3,".bc",3)) ftype = 1;
                  else if (n>4 &&  !strncmp(f2+n-5,"index",5)) ftype = 2;
               }
               if (!ftype) {
                  ftype = 1;  // assume file is a bc-file
//                   printf(" Failed to determine file type, aborting\n");
//                   ret = 1;
               }
            }
            if (!ret) {
               if ((ftype == 1 && mode > 1) || (ftype == 2 && !mode)) {
                  printf(" Selected mode is not compatible with input file type, aborting\n");
                  ret = 1;
               } else if (task==2 && !mode) {
                  printf(" Selected mode is not available for selected task, aborting\n");
                  ret = 1;
               }
            }
         }
      }
      if (!ret) {
         // load data
         if (ftype==1) {
            init_BcFile(&bf1);
            if (task==1) init_BcFile(&bf2);
            if ((n=read_bc(f1,&bf1,mode))) { 
               if (n==-1) printf(" Failed to open \"%s\" with read permissions, aborting\n",f1);
               else if (n==-2) printf(" File \"%s\" is not recognized as a bc-file (missing or erronous magic number)\n",f1);
               else if (n==-3) printf(" Failed to parse header block in \"%s\", aborting\n",f1);
               else if (n==-4) printf(" Failed to parse data block in \"%s\", aborting\n",f1);
               else printf(" Some unknown error occured (error code: %d) while reading \"%s\", aborting\n",n,f1);
               ret = 1;
            }
            if (!ret && task==1 && (n=read_bc(f2,&bf2,mode))) {
               if (n==-1) printf(" Failed to open \"%s\" with read permissions, aborting\n",f2);
               else if (n==-2) printf(" File \"%s\" is not recognized as a bc-file (missing or erronous magic number)\n",f2);
               else if (n==-3) printf(" Failed to parse header block in \"%s\", aborting\n",f2);
               else if (n==-4) printf(" Failed to parse data block in \"%s\", aborting\n",f2);
               else printf(" Some unknown error occured (error code: %d) while reading \"%s\", aborting\n",n,f2);
               free_BcFile(&bf1);
               ret = 1;
            }
         } else {
            init_BcIndex(&bi1);
            if (task==1) init_BcIndex(&bi2);
            if ((n=read_BcIndex(f1,&bi1))<0) { 
               if (n==-1) printf(" Failed to open \"%s\" with read permissions, aborting\n",f1);
               else if (n==-2) printf(" Failed to read/parse \"%s\", aborting\n",f1);
               else if (n==-3) printf(" Size of file \"%s\" differs from the expected (file is probably not an index file), aborting\n",f1);
               else printf(" Some unknown error occured (error code: %d) while reading \"%s\", aborting\n",n,f1);
               ret = 1;
            }
            if (!ret && task==1 && (n=read_BcIndex(f2,&bi2))<0) {
               if (n==-1) printf(" Failed to open \"%s\" with read permissions, aborting\n",f1);
               else if (n==-2) printf(" Failed to read/parse \"%s\", aborting\n",f1);
               else if (n==-3) printf(" Size of file \"%s\" differs from the expected (file is probably not an index file), aborting\n",f1);
               else printf(" Some unknown error occured (error code: %d) while reading \"%s\", aborting\n",n,f1);
               ret = 1;
            }
            if (!ret && mode == 2) {
               sort_BcIndex(&bi1);
               if (task==1) sort_BcIndex(&bi2);
            } else if (!ret && mode == 3) {
               n = clean_BcIndex(&bi1,0);
               if (n) printf(" Removed %d duplicate entries from %s\n",n,f1);
               else   printf(" No duplicate entries found in %s\n",f1);
               if (task==1) {
                  n = clean_BcIndex(&bi2,0);
                  if (n) printf(" Removed %d duplicate entries from %s\n",n,f2);
                  else   printf(" No duplicate entries found in %s\n",f2);
               }
            }
         }
         
         if (!ret) {
            if (!task) {
               // output content in file
               if (ftype==1) {
                  printf_BcFile(&bf1);
                  free_BcFile(&bf1);
               } else {
                  printf_BcIndex(&bi1);
                  free_BcIndex(&bi1);
               }
            } else if (task==1) {
               // compare content in file
               if (ftype==1) {
                  cmp_bc(&bf1, &bf2, 1);
                  free_BcFile(&bf1);
                  free_BcFile(&bf2);
               } else {
                  cmp_BcIndex(&bi1, &bi2, 1);
                  free_BcIndex(&bi1);
                  free_BcIndex(&bi2);
               }
            } else if (task==2) {
               // copy file
               if (ftype==1) {
                  if ((n=write_bc(f2,&bf1))) {
                     if (n==-1) printf(" Failed to open \"%s\" with write permission, aborting\n",f2);
                     else if (n==-2) printf(" Failed to write header block to \"%s\", aborting\n",f2);
                     else if (n==-3) printf(" Failed to write data block to \"%s\", aborting\n",f2);
                     else printf(" Failed to \"%s\" (error returned: %d), aborting\n",f2,n);
                     ret = 1;
                  } else printf(" Copied \"%s\" to \"%s\"\n",f1,f2);
                  free_BcFile(&bf1);
               } else {
                  if ((n=write_BcIndex(f2,&bi1))) {
                     if (n==-1) printf(" Failed to open \"%s\" with write permission, aborting\n",f2);
                     else if (n==-2) printf(" Failed to write file \"%s\", aborting\n",f2);
                     else if (n==-3) printf(" Refusing to write empty file \"%s\", aborting\n",f2);
                     else printf(" Failed to \"%s\" (error returned: %d), aborting\n",f2,n);
                     ret = 1;
                  } else printf(" Copied \"%s\" to \"%s\"\n",f1,f2);
                  free_BcIndex(&bi1);
               }
            }
         }
      }
   }
   return ret;
}
#endif
